/**
 * @file NimLearner.cpp
 */

#include "NimLearner.h"
#include <string>

/**
 * Constructor to create a game of Nim with `startingTokens` starting tokens.
 *
 * This function creates a graph, `g_` representing all of the states of a
 * game of Nim with vertex labels "p#-X", where:
 * - # is the current player's turn; p1 for Player 1, p2 for Player2
 * - X is the tokens remaining at the start of a player's turn
 *
 * For example:
 *   "p1-4" is Player 1's turn with four (4) tokens remaining
 *   "p2-8" is Player 2's turn with eight (8) tokens remaining
 *
 * All legal moves between states are created as edges with initial weights
 * of 0.
 *
 * @param startingTokens The number of starting tokens in the game of Nim.
 */
NimLearner::NimLearner(unsigned startingTokens) : g_(true) {
  for(int i = startingTokens; i>=0; i--){
    string label1 = "p1-"+std::to_string(i);
    string label2 = "p2-"+std::to_string(i);
    g_.insertVertex(label1);
    g_.insertVertex(label2);
  }
  startingVertex_ = g_.getVertexByLabel("p1-"+std::to_string(startingTokens));
  for(int j = startingTokens; j>0; j--){
    string this1 = "p1-"+std::to_string(j);
    string this2 = "p2-"+std::to_string(j);
    string next1 = "p1-"+std::to_string(j-1);
    string next2 = "p2-"+std::to_string(j-1);
     Vertex  thisp1 = g_.getVertexByLabel(this1);
     Vertex thisp2 = g_.getVertexByLabel(this2);
     Vertex nextp1 = g_.getVertexByLabel(next1);
     Vertex nextp2 = g_.getVertexByLabel(next2);
    g_.insertEdge(thisp1,nextp2);
    g_.setEdgeWeight(thisp1,nextp2,0);
    g_.insertEdge(thisp2,nextp1);
    g_.setEdgeWeight(thisp2,nextp1,0);
    if(j > 1){
      string dnext1 = "p1-"+std::to_string(j-2);
      string dnext2 = "p2-"+std::to_string(j-2);
      Vertex doublenextp1 = g_.getVertexByLabel(dnext1);
      Vertex doublenextp2 = g_.getVertexByLabel(dnext2);
      g_.insertEdge(thisp1, doublenextp2);
      g_.setEdgeWeight(thisp1,doublenextp2,0);
      g_.insertEdge(thisp2, doublenextp1);
      g_.setEdgeWeight(thisp2,doublenextp1,0);
    }
  }
}

/**
 * Plays a random game of Nim, returning the path through the state graph
 * as a vector of `Edge` classes.  The `origin` of the first `Edge` must be
 * the vertex with the label "p1-#", where # is the number of starting
 * tokens.  (For example, in a 10 token game, result[0].origin must be the
 * vertex "p1-10".)
 *
 * @returns A random path through the state space graph.
 */
std::vector<Edge> NimLearner::playRandomGame() const {
  vector<Edge> path;
  Vertex curr = startingVertex_;
//  std::cout<<g_.getVertexLabel(startingVertex_)<<endl;
  Vertex next;
  while((g_.getVertexLabel(curr) != "p1-0")&&(g_.getVertexLabel(curr) != "p2-0")) {
    next = g_.getAdjacent(curr)[rand() % (g_.getAdjacent(curr).size())];
    //std::cout<<g_.getVertexLabel(curr)<<endl;
    //std::cout<<g_.getVertexLabel(next)<<endl;
    path.push_back(g_.getEdge(curr,next));
    curr = next;
  }
  return path;
}


/*
 * Updates the edge weights on the graph based on a path through the state
 * tree.
 *
 * If the `path` has Player 1 winning (eg: the last vertex in the path goes
 * to Player 2 with no tokens remaining, or "p2-0", meaning that Player 1
 * took the last token), then all choices made by Player 1 (edges where
 * Player 1 is the source vertex) are rewarded by increasing the edge weight
 * by 1 and all choices made by Player 2 are punished by changing the edge
 * weight by -1.
 *
 * Likewise, if the `path` has Player 2 winning, Player 2 choices are
 * rewarded and Player 1 choices are punished.
 *
 * @param path A path through the a game of Nim to learn.
 */
void NimLearner::updateEdgeWeights(const std::vector<Edge> & path) {
    bool player2wins = (g_.getVertexLabel(path[path.size()-1].dest) == "p1-0");
    for(int i = 0; i< path.size(); i++){
      Vertex source = path[i].source;
      Vertex dest = path[i].dest;
      int weight = g_.getEdgeWeight(source,dest);
      string currplayer = (g_.getVertexLabel(source)).substr(0,2);
      if(player2wins){
        if(currplayer == "p2"){
          g_.setEdgeWeight(source,dest,weight+1);
        }else if(currplayer == "p1"){
          g_.setEdgeWeight(source,dest,weight-1);
        }
      }else{
        if(currplayer == "p1"){
          g_.setEdgeWeight(source,dest,weight+1);
        }else if(currplayer == "p2"){
          g_.setEdgeWeight(source,dest,weight-1);
        }
      }
    }
}


/**
 * Returns a constant reference to the state space graph.
 *
 * @returns A constant reference to the state space graph.
 */
const Graph & NimLearner::getGraph() const {
  return g_;
}